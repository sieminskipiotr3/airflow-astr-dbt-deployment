{{ config(materialized='table', unique_key='listing_id') }}


SELECT

    listing_id
    , listing_name
    , listing_type
    , square_feet
    , price
    , listing_state
    
FROM {{source('raw_source', 'listings')}}
