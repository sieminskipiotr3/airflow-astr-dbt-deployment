{{ config(materialized='table', unique_key='view_id') }}

WITH property_views AS (
    SELECT
        generate_series(1, 50000) AS view_id
        , floor(random() * 10000 + 1)::INT AS property_id
        , floor(random() * 10000 + 1)::INT AS listing_id
        , floor(random() * 10000 + 1)::INT AS customer_id
        , current_timestamp - (random() * (interval '1 day')) AS view_time
        , current_timestamp AS inserted_ts
)
SELECT * FROM property_views
