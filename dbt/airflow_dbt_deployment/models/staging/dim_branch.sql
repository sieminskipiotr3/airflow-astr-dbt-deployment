{{ config(materialized='table', unique_key='branch_id') }}

WITH branches AS (
    SELECT
        generate_series(1, 100) AS branch_id
        , md5(random()::text) AS branch_name
        , floor(random() * 500 + 1)::INT AS agent_id
)
SELECT * FROM branches
