{{ config(materialized='table', unique_key='your_unique_key_here') }}

WITH favorites AS (
    SELECT
        generate_series(1, 10000) AS favorite_id
        , (random() * 10000)::int AS user_id
        , (random() * 10000)::int AS listing_id
        , now() - (random() * (30 || ' days')::interval) AS favorited_on
)

, adding_pk AS (
    SELECT
        *
        , {{ dbt_utils.generate_surrogate_key(['favorite_id', 'user_id', 'listing_id']) }} AS pk
    FROM favorites
)

SELECT * FROM adding_pk
