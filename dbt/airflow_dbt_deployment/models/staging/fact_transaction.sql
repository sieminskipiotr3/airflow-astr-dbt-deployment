{{ config(
    materialized = "table",
    unique_key = 'transaction_id',
	tags = ['finance', 'transaction']
) }}

WITH transactions AS (
    SELECT
        generate_series(1, 10000) AS transaction_id
        , floor(random() * 10000 + 1)::INT AS listing_id
        , floor(random() * 500 + 1)::INT AS agent_id
        , floor(random() * 10000 + 1)::INT AS customer_id
        , ROUND(100000 + (RANDOM() * (20000000 - 100000))) AS transaction_amount
        , current_date - (floor(random() * 30))::INT AS transaction_date
        , current_timestamp AS inserted_ts
)

SELECT * FROM transactions
