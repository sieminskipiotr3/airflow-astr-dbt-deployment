{{ config(materialized='table', unique_key='page_id') }}

WITH webpages AS (
    SELECT
        generate_series(1, 200) AS page_id
        , md5(random()::text) AS page_url
)
SELECT * FROM webpages
