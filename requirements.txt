dbt-core==1.7.0
dbt-postgres==1.7.0
psycopg2-binary
pandas
pyarrow
astronomer-cosmos==1.0.4
apache-airflow-providers-postgres==5.6.0