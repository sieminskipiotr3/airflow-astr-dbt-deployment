{{ config(
    materialized = "incremental",
    unique_key = 'transaction_id',
    on_schema_change = 'sync_all_columns',
    incremental_strategy='delete+insert',
	tags = ['property', 'finance', 'transaction', 'reporting']
) }}

WITH property_transactions AS (

    SELECT

        t.transaction_id
        , t.transaction_date
        , t.transaction_amount
        , t.inserted_ts
        , l.listing_id
        , l.listing_name
        , l.price AS original_price
        , a.agent_id
        , a.agent_name
        , COALESCE(b.branch_id, 1) AS branch_id
        , COALESCE(b.branch_name, 'online') AS branch_name

    FROM {{ ref('fact_transaction') }} AS t

    LEFT JOIN {{ ref('dim_listing') }} AS l 
        USING(listing_id)

    LEFT JOIN {{ ref('dim_agent') }} AS a 
        USING(agent_id)

    LEFT JOIN {{ ref('dim_branch') }} AS b 
        USING(agent_id)

    WHERE 1=1
    {% if is_incremental() %}
        AND t.inserted_ts > (SELECT MAX(th.inserted_ts) FROM {{ this }} th)
    {% endif %}

)

SELECT

    transaction_id
    , transaction_date
    , transaction_amount
    , inserted_ts
    , listing_id
    , listing_name
    , original_price
    , agent_id
    , agent_name
    , branch_id
    , branch_name
    
FROM property_transactions
