{{ config(
    materialized = "table",
    unique_key = 'interaction_id',
	tags = ['seekers', 'interaction']
) }}

SELECT 

    interaction_id
    , seeker_id
    , event_type
    , event_timestamp
    , session_id
    , listing_id
    , browser
    , operating_system
    , device_type
    , referral_source
    , query_text
    , session_start
    , session_end
    , page_id
    , duration_seconds 
    , page_view_duration_seconds
    , is_new_visitor
    , page_visited
    , scroll_depth
    , is_conversion
    , seeker_agent
    , current_timestamp AS inserted_ts

FROM {{source('raw_source', 'seekers')}}