# Use astronomer image
FROM quay.io/astronomer/astro-runtime:9.5.0

USER root

# Install Python dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

RUN python -m venv dbt_venv && source dbt_venv/bin/activate && \
    pip install --no-cache-dir dbt-postgres && deactivate

USER astro

ENV POSTGRES_CONNECTOR=postgres://psieminski:mypassword@host.docker.internal:5432/public
ENV POSTGRES_CONN_ID=postgresql://psieminski:mypassword@host.docker.internal:5432/piotrsieminski
ENV DBT_PROJECT_DIR=/usr/local/airflow/dbt/airflow_dbt_deployment
ENV DBT_DOCS_PATH=/usr/local/airflow/dbt-docs
ENV helpers=/usr/local/airflow/helpers

ENV DBT_USER=psieminski