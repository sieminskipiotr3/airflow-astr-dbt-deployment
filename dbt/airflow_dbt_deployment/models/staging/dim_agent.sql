{{ config(materialized='table', unique_key='agent_id') }}

WITH agents AS (
    SELECT
        generate_series(1, 500) AS agent_id
        , md5(random()::text) AS agent_name
)
SELECT * FROM agents
