#!/bin/bash

# This script is intended for MacOS and assumes python and homebrew installed
# If you do not have them, please install before executing the script

# Pull the Postgres image
docker pull postgres

# Check for Astro CLI installation
if ! command -v astro &> /dev/null
then
    echo "Astro CLI could not be found, installing..."
    curl -sSL https://install.astronomer.io | sudo bash
fi

# Check for virtualenv installation
if ! command -v virtualenv &> /dev/null
then
    echo "Virtualenv could not be found, installing..."
    pip3 install virtualenv
fi

# Create and activate virtual environment
VIRTUAL_ENV_DIR="venv"
if [ ! -d "$VIRTUAL_ENV_DIR" ]
then
    python3 -m venv $VIRTUAL_ENV_DIR
fi
source $VIRTUAL_ENV_DIR/bin/activate

# Install Python dependencies
pip install -r requirements.txt

# Start the environment using astro CLI
astro dev start

# Wait for the Postgres container to be up
MAX_ATTEMPTS=5
ATTEMPTS=0
POSTGRES_UP=false

while [ $ATTEMPTS -lt $MAX_ATTEMPTS ]; do
    POSTGRES_CONTAINER=$(docker ps --filter "ancestor=postgres" --format "{{.Names}}" | head -n 1)
    if [ -n "$POSTGRES_CONTAINER" ] && docker exec $POSTGRES_CONTAINER pg_isready -U postgres; then
        POSTGRES_UP=true
        break
    else
        echo "Waiting for Postgres container to be up..."
        sleep 30
        ((ATTEMPTS++))
    fi
done

if [ "$POSTGRES_UP" = false ]; then
    echo "Postgres container did not start within expected time. Exiting."
    exit 1
fi

# Define database details
DB_NAME="piotrsieminski"
DB_USER="psieminski"
DB_PASSWORD="mypassword"
DB_SCHEMA="public"

# Check if the database already exists
if docker exec $POSTGRES_CONTAINER psql -U postgres -tAc "SELECT 1 FROM pg_database WHERE datname='$DB_NAME'" | grep -q 1; then
    echo "Database $DB_NAME already exists. Skipping creation."
else
    echo "Database $DB_NAME does not exist. Creating database, user, role, and schema..."
    docker exec -i $POSTGRES_CONTAINER psql -U postgres <<-EOSQL
        CREATE USER $DB_USER WITH PASSWORD '$DB_PASSWORD';
        CREATE DATABASE $DB_NAME;
        GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;
        \c $DB_NAME
        CREATE SCHEMA $DB_SCHEMA AUTHORIZATION $DB_USER;
EOSQL
fi
