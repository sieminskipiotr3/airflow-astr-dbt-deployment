{{ config(
    materialized = "incremental",
    unique_key = 'pk',
    on_schema_change = 'sync_all_columns',
    incremental_strategy='delete+insert',
	tags = ['seeker', 'interaction', 'reporting']
) }}

WITH seeker_interactions AS (
    
    SELECT

        si.interaction_id
        , si.seeker_id
        , si.event_type
        , si.event_timestamp
        , DATE(si.event_timestamp) AS event_date
        , si.session_id
        , si.browser
        , si.operating_system
        , si.device_type
        , si.referral_source
        , si.query_text
        , si.session_start
        , si.session_end
        , (si.session_end - si.session_start) AS session_duration_seconds
        , si.page_id
        , si.duration_seconds AS interaction_duration_seconds
        , si.page_view_duration_seconds
        , si.is_new_visitor
        , si.page_visited
        , si.scroll_depth
        , si.is_conversion
        , si.inserted_ts
        , c.customer_id
        , l.listing_id
        , l.listing_type
        , l.listing_state
        , l.square_feet AS listing_square_feet
        , l.price AS listing_price
        , pv.listing_views_count

    FROM {{ ref('fact_seekers_interactions') }} AS si

    LEFT JOIN {{ ref('dim_customer') }} AS c 
        USING(seeker_id)

    LEFT JOIN {{ ref('dim_listing') }} AS l 
        USING(listing_id)

    LEFT JOIN (

        SELECT 

            listing_id
            , COUNT(*) AS listing_views_count

        FROM {{ ref('fact_property_views') }}
        GROUP BY listing_id

    ) AS pv USING(listing_id)

    WHERE 1=1
    {% if is_incremental() %}
        AND si.inserted_ts > (SELECT MAX(t.inserted_ts) FROM {{ this }} t)
    {% endif %}

)

, adding_pk AS (

    SELECT

        *
        , {{ dbt_utils.generate_surrogate_key(['interaction_id'
                                        , 'seeker_id'
                                        , 'customer_id'
                                        , 'session_id']) }} AS pk

    FROM seeker_interactions

)

SELECT

    interaction_id
    , seeker_id
    , event_type
    , event_timestamp
    , event_date
    , session_id
    , browser
    , operating_system
    , device_type
    , referral_source
    , query_text
    , session_start
    , session_end
    , session_duration_seconds
    , page_id
    , interaction_duration_seconds
    , page_view_duration_seconds
    , is_new_visitor
    , page_visited
    , scroll_depth
    , is_conversion
    , inserted_ts
    , customer_id
    , listing_id
    , listing_type
    , listing_state
    , listing_square_feet
    , listing_price
    , listing_views_count
    , pk

FROM adding_pk
