import psycopg2
import pandas as pd
import os

# Function to map Pandas dtype to SQL
def dtype_to_sql(dtype):
    if dtype == 'int64':
        return 'INTEGER'
    elif dtype == 'float':
        return 'FLOAT'
    elif dtype == 'object':
        return 'TEXT'
    elif dtype == 'datetime64[ns, UTC]':
        return 'TIMESTAMP'
    elif dtype == 'bool':
        return 'BOOLEAN'
    else:
        return 'TEXT'

# Function to create SQL table
def create_table_sql(df, table_name, schema_name):
    columns_sql = ", ".join([f"{col} {dtype_to_sql(df[col].dtype)}" for col in df.columns])
    
    drop_table_query = f"DROP TABLE IF EXISTS {schema_name}.{table_name};"
    create_table_query = f"CREATE TABLE {schema_name}.{table_name} ({columns_sql});"
    
    return drop_table_query + create_table_query

def upload_parquet_files():
    # postgres_conn_uri = BaseHook.get_connection(kwargs['POSTGRES_CONN_ID']).get_uri()
    postgres_conn_uri = 'postgresql://psieminski:mypassword@host.docker.internal:5432/piotrsieminski'
    file_paths = ['/usr/local/airflow/data_samples/listings.parquet'
                  , '/usr/local/airflow/data_samples/seekers.parquet']
    for file_path in file_paths:
        df = pd.read_parquet(file_path)

        # Generate table name from file path
        table_name = os.path.basename(file_path).split('.')[0]

        # Construct SQL for table creation
        create_table_query = create_table_sql(df, table_name, 'public')

        # Create connection and execute create table query
        with psycopg2.connect(postgres_conn_uri) as conn:
            with conn.cursor() as cur:
                cur.execute(create_table_query)
                conn.commit()

        # Upload DataFrame to SQL
        df.to_sql(table_name, con=postgres_conn_uri, if_exists='append', index=False, method='multi')