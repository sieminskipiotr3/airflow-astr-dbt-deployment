import sys
sys.path.append('/usr/local/airflow')
from pendulum import datetime
import os
from cosmos import DbtTaskGroup, ProjectConfig, ProfileConfig
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.decorators import dag
from airflow.operators.python_operator import PythonOperator
from helpers.parquet_upload import upload_parquet_files

DBT_PROJECT_DIR = os.environ.get('DBT_PROJECT_DIR')
DBT_PROFILES_PATH = os.environ.get('DBT_PROFILES_PATH') 
DBT_PROFILE_NAME = os.environ.get('DBT_PROFILE_NAME') 
DBT_TARGET = os.environ.get('DBT_TARGET') 

profile_config = ProfileConfig( 
    profile_name=DBT_PROFILE_NAME, 
    target_name=DBT_TARGET, 
    profiles_yml_filepath=DBT_PROFILES_PATH,
) 

default_args = {
    'owner': 'airflow',
    'retries': 1,
}

@dag(
    start_date=datetime(2023, 11, 1),
    schedule_interval="@hourly",
    catchup=False,
)
def dbt_run_dag():
    transform_data = DbtTaskGroup(
        group_id="transform_data",
        project_config=ProjectConfig(DBT_PROJECT_DIR,),
        profile_config=profile_config,
        operator_args={ 
            "install_deps": True, 
        }, 
        default_args=default_args,
    )

    grant_privileges_task = PostgresOperator(
        task_id='grant_all_privileges',
        postgres_conn_id='POSTGRES_CONNECTOR', 
        sql="GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO psieminski;",
    )

    upload_task = PythonOperator(
        task_id='upload_parquet_files',
        python_callable=upload_parquet_files,
        provide_context=True,
        op_kwargs={'postgres_conn_id': 'POSTGRES_CONNECTOR'}, 
    )

    upload_task >> grant_privileges_task >> transform_data


dbt_run_dag()