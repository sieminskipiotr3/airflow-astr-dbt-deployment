# Real Estate Data Engineering Pipeline with Airflow and dbt

## Overview
Developed a comprehensive data engineering pipeline for a real estate company, focusing on property listings and web interaction data. The project leverages Apache Airflow, dbt (data build tool), and PostgreSQL, orchestrated in a containerized environment via Astronomer.

## Objectives
- **Data Ingestion and Processing:** Automate the ingestion and processing of real estate listings and user interaction data for analytics purposes.
- **Pipeline Orchestration:** Implement a scalable workflow using Apache Airflow, managed by Astronomer, for task orchestration.
- **Data Transformation:** Use dbt for robust data transformation, ensuring consistency and integrity.

## Key Features
- **Data Models:** Designed 10 data entities (e.g., `dim_customer`, `fact_transaction`) to structure the data efficiently, generating 10K dummy property listings and 100K user interaction records.
- **Airflow DAGs:** Custom DAGs for managing dbt model executions and data processing tasks.
- **Containerized Deployment:** Deployed in Docker, integrating Airflow, dbt, and PostgreSQL for consistent development and production setups.

## Technologies Used
- **Workflow Orchestration:** Apache Airflow
- **Data Modeling & Transformation:** dbt
- **Database Management:** PostgreSQL
- **Containerization & Environment Management:** Docker, Astronomer

## Outcomes
The automated pipeline significantly reduces manual efforts, enhancing data reliability and enabling timely insights for improved decision-making in property listings and user engagement strategies.

# Project Setup Instructions

This document provides step-by-step instructions for setting up the project environment, and ensuring that Docker and Python are installed on your machine. This guide is intended for Mac/Linux environments with zsh terminal. You can adjust the commands/steps as per requirements of your OS.

## Part 1: Setting Up Project Environment

### Clone Your GitLab Repository
- Open your terminal.
- Navigate to the directory where you want to clone the repository.
- Use the command `git clone [URL-of-your-GitLab-repo]`. Replace `[URL-of-your-GitLab-repo]` with your repository URL.

#### Install Docker (if not installed)
- Visit the Docker website (https://www.docker.com/products/docker-desktop).
- Download Docker Desktop for your operating system.
- Follow the installation instructions provided by Docker.

#### Verify Docker Installation
- After installation, run `docker --version` to ensure it’s correctly installed.

## Credentials

Modify the `.env` and Dockerfile to include necessary credentials for your local deployment

## Automated deployment

- This assumes that you have python, Docker and homebrew installed on your machine (script is intended for MacOS)
- Make `setup_and_deploy.sh` file executable in the root folder of this repository: `chmod +x setup_and_deploy.sh`
- Execute the file in the root folder of this repository: `./setup_and_deploy.sh`

## Manually start/stop deployment

- Execute `astro dev start` to initiate the Docker image and deploy Airflow locally
- Execute `astro dev stop` to stop deployment

##### License, copyrights, credits

1. Avatar logo of the project: <a href="https://www.freepik.com/icons/real-estate">Icon by xnimrodx</a>