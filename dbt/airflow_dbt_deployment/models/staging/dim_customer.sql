{{ config(materialized='table', unique_key='customer_id') }}

WITH customers AS (
    SELECT
        generate_series(1, 10000) AS customer_id
        , generate_series(1, 10000) AS seeker_id
        , md5(random()::text) AS customer_name
        , md5(random()::text) || '@example.com' AS email
        , ('+1-' || floor(random() * (9999999999)::bigint)::text) AS phone_number
)
SELECT * FROM customers
